FROM docker:24.0.4-dind
MAINTAINER Ertugrul Sener <ertugrulsener@hotmail.de>

# Download jdk
RUN apk add --no-cache openjdk17-jre-headless
RUN mkdir workspace

# Download and setup gradle
RUN wget https://services.gradle.org/distributions/gradle-8.2.1-bin.zip \
	&& unzip gradle-8.2.1-bin.zip \
	&& rm gradle-8.2.1-bin.zip \
	&& mv gradle-8.2.1 workspace

ENV GRADLE_HOME=/workspace/gradle-8.2.1
ENV PATH=$PATH:$GRADLE_HOME/bin

CMD ["sh"]